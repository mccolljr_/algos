package mergesort

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestSmallSort(t *testing.T) {
	list := []int{2, 1, 5, 6, 4, 3}
	list2 := MergeSort(list)

	for i, v := range list2 {
		if v != (i + 1) {
			t.FailNow()
		}
	}
}

func TestReverseSort(t *testing.T) {
	list := []int{6, 5, 4, 3, 2, 1}
	list2 := MergeSort(list)

	for i, v := range list2 {
		if v != (i + 1) {
			fmt.Printf("Index %v is %v\n", i, v)
			t.FailNow()
		}
	}
}

func TestHugeSort(t *testing.T) {
	size := rand.Intn(10000000)
	list := make([]int, size)
	for i := 0; i < size; i++ {
		list[i] = rand.Intn(size)
	}

	list2 := MergeSort(list)

	for i := 0; i < size-1; i++ {
		if list2[i] > list2[i+1] {
			fmt.Printf("%v should come before %v", list2[i+1], list2[i])
			t.FailNow()
		}
	}
}

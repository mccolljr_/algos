package mergesort

// MergeSort takes a slice of integers and sorts it in ascending order
// using the MergeSort algorithm.
func MergeSort(src []int) []int {
	size := len(src)

	if size == 1 {
		return src
	}

	half := size / 2
	a := src[:half]
	b := src[half:]

	a = MergeSort(a)
	b = MergeSort(b)

	res := []int{}

	for len(a) != 0 && len(b) != 0 {
		if a[0] < b[0] {
			res = append(res, a[0])
			a = a[1:]
		} else {
			res = append(res, b[0])
			b = b[1:]
		}
	}

	for len(a) != 0 {
		res = append(res, a[0])
		a = a[1:]
	}

	for len(b) != 0 {
		res = append(res, b[0])
		b = b[1:]
	}

	return res
}

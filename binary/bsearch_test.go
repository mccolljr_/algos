package binarysearch

import "testing"

type TestCase struct {
	target   int
	arr      []int
	expected int
}

var cases = []TestCase{
	{5, []int{0, 2, 3, 4, 5, 7}, 4},
	{10, []int{0, 2, 3, 4, 5, 10}, 5},
	{22, []int{0, 2, 3, 4, 5, 10}, -1},
	{10, []int{0, 2, 3, 4, 10}, 4},
	{3, []int{0, 2, 3, 4, 10}, 2},
	{4, []int{3, 4}, 1},
	{9, []int{0, 5, 9}, 2},
	{4, []int{4, 5}, 0},
	{9, []int{9, 15, 19}, 0},
	{7, []int{}, -1},
}

func TestRecursive(t *testing.T) {
	for _, tc := range cases {
		result := Recursive(tc.target, 0, tc.arr)

		if result != tc.expected {
			t.Errorf("Wanted %v but got %v", tc.expected, result)
		}
	}
}

func TestIterative(t *testing.T) {
	for _, tc := range cases {
		result := Iterative(tc.target, tc.arr)

		if result != tc.expected {
			t.Errorf("Wanted %v but got %v", tc.expected, result)
		}
	}
}

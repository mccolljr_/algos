package binarysearch

// Recursive performs a binary search on the given slice
// by recursively calling itself with a reduced slice
// if a match is found, the index in the original slice
// is returned, otherwise -1 is returned
func Recursive(target, offset int, slice []int) int {
	length := len(slice)

	// this handles the edge case where an empty slice is passed in
	if length == 0 {
		return -1
	}

	// base case for the recursion
	if length == 1 {
		if slice[0] == target {
			return offset
		}

		return -1
	}

	// handle binary search logic
	mid := (length / 2)

	if target > slice[mid] {
		return Recursive(target, offset+mid, slice[mid:])
	} else if target < slice[mid] {
		return Recursive(target, offset, slice[:mid])
	} else {
		return mid + offset
	}
}

// Iterative performs a binary search on the given slice
// by recursively looping over subslices
// if a match is found, the index in the original slice
// is returned, otherwise -1 is returned
func Iterative(target int, slice []int) int {

	result := -1
	offset := 0

	for {
		length := len(slice)

		// this handles the edge case where an empty slice is passed in
		if length == 0 {
			return -1
		}

		// base case for the recursion
		if length == 1 {
			if slice[0] == target {
				result = offset
			}
			break
		}

		// handle binary search logic
		mid := (length / 2)

		if target > slice[mid] {
			offset += mid
			slice = slice[mid:]
		} else if target < slice[mid] {
			slice = slice[:mid]
		} else {
			result = mid + offset
			break
		}
	}

	return result
}

package main

import "fmt"

type element struct {
	key  int
	data interface{}
}

type maxHeap []*element

func (h *maxHeap) insert(key int, data interface{}) {
	ne := &element{key, data}

	*h = append(*h, ne)

	h.fix(len(*h) - 1)
}

func (h *maxHeap) swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
}

func (h *maxHeap) bubble(i int) {
	if h.less(i, i/2) {
		h.swap(i, i/2)
		h.bubble(i / 2)
	}
}

func (h *maxHeap) sink(i, n int) {
	for {
		// left child of I
		j1 := 2*i + 1

		if j1 >= n || j1 < 0 { // j1 < 0 after int overflow
			break
		}
		j := j1 // left child

		// if the right child is within range, and
		// has a value less than or equal to the left child,
		// we need to look at the right child.
		if j2 := j1 + 1; j2 < n && !h.less(j1, j2) {
			j = j2 // = 2*i + 2  // right child
		}
		if !h.less(j, i) {
			break
		}
		h.swap(i, j)
		i = j
	}
}

func (h *maxHeap) fix(i int) {
	h.sink(i, len(*h))
	h.bubble(i)
}

func (h *maxHeap) pop() *element {
	n := len(*h) - 1
	h.swap(0, n)
	h.sink(0, n)
	el := (*h)[n]
	(*h) = (*h)[:n]
	return el
}

func (h *maxHeap) less(i, j int) bool {
	return (*h)[j].key < (*h)[i].key
}

func main() {
	h := &maxHeap{}

	h.insert(2, nil)
	h.insert(4, nil)
	h.insert(1, nil)
	h.insert(5, nil)
	h.insert(0, nil)

	h.pop()
	h.pop()
	h.print() // should only have 3 elements: 2, 1, and 0
	h.pop()
	h.print() // should only have 2 elements: 1, 0
	h.pop()
	h.print() // should only have 1 element with key of 0
}

func (h *maxHeap) print() {
	for _, e := range *h {
		fmt.Println(e)
	}

	fmt.Println()
}

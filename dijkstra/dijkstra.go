package main

// TODO: refactor & document

import (
	"container/heap"
	"fmt"
	"math"
)

type node struct {
	name      string
	distance  int
	done      bool
	prev      *node
	neighbors []neighbor
	rx        int // index in queue
}

type edge struct {
	start  string
	end    string
	weight int
}

type neighbor struct {
	node     *node
	distance int
}

type nodeList []*node

func newNodeList(hint int) *nodeList {
	l := make([]*node, 0, hint)
	return (*nodeList)(&l)
}

func (nl nodeList) Less(idx1, idx2 int) bool {
	// we want lower-valued nodes to be ordered before
	if nl[idx1].distance < nl[idx2].distance {
		return true
	}

	return false
}

func (nl nodeList) Len() int {
	return len(nl)
}

func (nl nodeList) Swap(idx1, idx2 int) {
	nl[idx1], nl[idx2] = nl[idx2], nl[idx1]

	nl[idx2].rx = idx2
	nl[idx1].rx = idx1
}

func (nl *nodeList) Push(n interface{}) {
	*nl = append(*nl, n.(*node))
}

func (nl *nodeList) Pop() interface{} {
	last := len(*nl) - 1
	result := (*nl)[last]
	*nl = (*nl)[:last]
	return result
}

func buildGraph(edges []edge, directed bool, start, end string) (result []*node, startn, endn *node) {
	nodes := make(map[string]*node)

	for _, e := range edges {
		if nodes[e.start] == nil {
			nodes[e.start] = &node{
				name:      e.start,
				distance:  math.MaxInt32,
				done:      false,
				prev:      nil,
				neighbors: make([]neighbor, 0, 100),
				rx:        -1,
			}

			if e.start == start {
				startn = nodes[e.start]
			} else if e.start == end {
				endn = nodes[e.start]
			}
		}

		if nodes[e.end] == nil {
			nodes[e.end] = &node{
				name:      e.end,
				distance:  math.MaxInt32,
				done:      false,
				prev:      nil,
				neighbors: make([]neighbor, 0, 100),
				rx:        -1,
			}

			if e.end == start {
				startn = nodes[e.end]
			} else if e.end == end {
				endn = nodes[e.end]
			}
		}

		nodes[e.start].neighbors = append(nodes[e.start].neighbors, neighbor{nodes[e.end], e.weight})

		if !directed {
			nodes[e.end].neighbors = append(nodes[e.end].neighbors, neighbor{nodes[e.start], e.weight})
		}
	}

	result = make([]*node, len(nodes))

	index := 0
	for _, n := range nodes {
		result[index] = n
		index++
	}

	return
}

func main() {
	graph, start, end := buildGraph([]edge{
		{"a", "d", 11},
		{"a", "b", 3},
		{"b", "c", 3},
		{"c", "d", 7},
	}, false, "a", "d")

	path := dijkstra(graph, start, end)

	for _, n := range path {
		fmt.Printf(" -> %+v", n.name)
	}

	fmt.Println()
}

func dijkstra(all []*node, start, end *node) []*node {
	available := newNodeList(1)

	start.distance = 0

	currentNode := start

	for {

		if currentNode == end {
			break
		}

		for _, n := range currentNode.neighbors {
			if !n.node.done {
				if d := currentNode.distance + n.distance; d < n.node.distance {
					n.node.distance = d
					n.node.prev = currentNode

					if n.node.rx < 0 {
						heap.Push(available, n.node)
					} else {
						heap.Fix(available, n.node.rx)
					}
				}
			}
		}

		if available.Len() == 0 {
			break
		}

		currentNode.done = true

		currentNode = heap.Pop(available).(*node)
	}

	result := make([]*node, 0, 30)

	for currentNode.prev != nil {
		result = append([]*node{currentNode}, result...)
		currentNode = currentNode.prev
	}

	return result
}

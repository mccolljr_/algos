package trie

type node struct {
	value    interface{} // can be replaced to generate generics
	children map[rune]*node
}

func newNode(v interface{}) *node {
	return &node{
		value:    v,
		children: make(map[rune]*node),
	}
}

type Trie struct {
	root *node
}

func New() *Trie {
	return &Trie{
		root: newNode(nil),
	}
}

func (t Trie) Put(key string, val interface{}) {
	krunes := []rune(key) // conversion is O(N)

	n := t.root
	i := 0
	k := len(krunes)

	for i < k {
		if nn, ok := n.children[krunes[i]]; ok {
			n = nn
			i++
		} else {
			break
		}
	}

	for i < k {
		n.children[krunes[i]] = newNode(nil)
		n = n.children[krunes[i]]
		i++
	}

	n.value = val
}

func (t Trie) Get(key string) interface{} {
	node := t.root
	for _, r := range key {
		if nn, ok := node.children[r]; ok {
			node = nn
		} else {
			return nil
		}
	}

	if node.value != nil {
		return node.value
	}

	return nil
}

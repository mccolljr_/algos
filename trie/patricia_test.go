package trie

import "testing"

func TestSplitWithPrefixLen(t *testing.T) {
	cases := []struct {
		n      *pnode
		at     int
		s1, s2 string
	}{
		{
			newPNode("test", 1),
			prefixLen("test", "te"),
			"te", "st",
		},
		{
			newPNode("wacom", 1),
			prefixLen("wacom", "waco"),
			"waco", "m",
		},
		{
			newPNode("test", 1),
			prefixLen("test", "test"),
			"test", "",
		},
	}

	for _, c := range cases {
		sp1, sp2 := splitNode(c.at, c.n)

		if sp1.prefix != c.s1 {
			t.Errorf("Wanted new parent to have prefix %s, but got %s", c.s1, sp1.prefix)
		}

		if sp2.prefix != c.s2 {
			t.Errorf("Wanted new child to have prefix %s, but got %s", c.s2, sp2.prefix)
		}

		if c.n.val != sp2.val {
			t.Errorf("Wanted split child to have value of original node (%v), but got %v", c.n.val, sp2.val)
		}
	}
}

func TestPatriciaPutGet(t *testing.T) {
	p := NewPatricia()

	// the following list of words touches on each condition during the insertion process
	// tester extends test,
	// tess splits test into [tes, t] & extends tes,
	// te splits tes,
	// xslt adds a new child to the root, next to tes,
	// musl adds a new child to the root, next to tes & xslt,
	// and mashgg splits musl into [m, usl] and extends m
	cases := []string{"test", "tester", "tess", "te", "xslt", "tes", "musl", "mashgg", "" /* root */}

	for i, c := range cases {
		p.Put(c, i)
	}

	for i, c := range cases {
		x := p.Get(c)

		if iv, ok := x.(int); !ok || iv != i {
			t.Errorf("Expected Get(%q) to return %d, got %v", c, i, x)
		}
	}
}

func TestPatriciaPut_New(t *testing.T) {
	p := NewPatricia()

	p.Put("test", 1)

	// the structure should be like this:
	// 				(root)
	//			   ("test")

	if lr := len(p.root.edges); lr != 1 {
		t.Errorf("Expected root node to have 1 edge, but found %d (%v)", lr, p.root.edges)
	}

	rchild := p.root.edges[0]

	if lr := len(rchild.edges); lr != 0 {
		t.Errorf("Expected the inserted node to have no children, but got %d (%v)", lr, rchild.edges)
	}

	if iv, ok := rchild.val.(int); !ok || iv != 1 {
		t.Errorf("Expected the inserted node to have value of 1, but got %v", rchild.val)
	}

	if rchild.prefix != "test" {
		t.Errorf("Expected the inserted node to have prefix of \"test\", but got %q", rchild.prefix)
	}
}

func TestPatriciaPut_Extend(t *testing.T) {
	p := NewPatricia()

	cases := []string{"test", "testy", "tester"}

	for i, c := range cases {
		p.Put(c, i)
	}

	// the structure should be like this:
	// 				(root)
	//			   ("test")
	//			 ("y") ("er")

	if lr := len(p.root.edges); lr != 1 {
		t.Errorf("Expected root node to have 1 edge, but found %d (%v)", lr, p.root.edges)
	}

	rchild := p.root.edges[0]

	if rchild.prefix != "test" {
		t.Errorf("Expected the first inserted node to have prefix \"test\", but have %q", rchild.prefix)
	}

	if lr := len(rchild.edges); lr != 2 {
		t.Errorf("Expected \"test\" node to have 2 children, but have %d (%v)", lr, rchild.edges)
	}

	seenPrefixes := map[string]bool{"y": false, "er": false}

	for _, e := range rchild.edges {
		seenPrefixes[e.prefix] = true
		lr := len(e.edges)

		if lr != 0 {
			t.Errorf("Expected second-level child %q to have no children, found %d", e.prefix, lr)
		}
	}

	for k, v := range seenPrefixes {
		if k == "y" || k == "er" {
			if !v {
				t.Errorf("Expected to have seen %q, but didn't", k)
			}
		} else {
			t.Errorf("Found unexpected prefix %q", k)
		}
	}

	for i, c := range cases {
		x := p.Get(c)

		if iv, ok := x.(int); !ok || iv != i {
			t.Errorf("Expected Get(%q) to return %d, got %v", c, i, x)
		}
	}
}

func TestPatriciaPut_Split(t *testing.T) {
	p := NewPatricia()

	cases := []string{"" /* root */, "test", "tease"}

	for i, c := range cases {
		p.Put(c, i)
	}

	// the structure should be like this:
	// 				(root)
	//				("te")
	//			("st") ("ase")

	if lr := len(p.root.edges); lr != 1 {
		t.Errorf("Expected root node to have 1 edge, but found %d (%v)", lr, p.root.edges)
	}

	rchild := p.root.edges[0]

	if rcpx := rchild.prefix; rcpx != "te" {
		t.Errorf("Expected the child of root to have prefix \"te\", but got %q", rcpx)
	}

	if lr := len(rchild.edges); lr != 2 {
		t.Errorf("Expected \"te\" node to have 2 edges, but found %d, (%v)", lr, rchild.edges)
	}

	seenPrefixes := map[string]bool{"st": false, "ase": false}

	for _, e := range rchild.edges {
		seenPrefixes[e.prefix] = true
		lr := len(e.edges)

		if lr != 0 {
			t.Errorf("Expected second-level child %q to have no children, found %d", e.prefix, lr)
		}
	}

	for k, v := range seenPrefixes {
		if k == "st" || k == "ase" {
			if !v {
				t.Errorf("Expected to have seen %q, but didn't", k)
			}
		} else {
			t.Errorf("Found unexpected prefix %q", k)
		}
	}

	for i, c := range cases {
		x := p.Get(c)

		if iv, ok := x.(int); !ok || iv != i {
			t.Errorf("Expected Get(%q) to return %d, got %v", c, i, x)
		}
	}
}

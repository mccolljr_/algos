package trie

const (
	defaultEdgeCount = 10
)

// prefixLen finds the length of the longest shared prefix between the two strings
func prefixLen(s1, s2 string) int {
	max := len(s1)

	if l := len(s2); l < max {
		max = l
	}

	var i int
	for i = 0; i < max; i++ {
		if s1[i] != s2[i] {
			break
		}
	}

	return i
}

// splitNode takes an index and a node, and splits the node into 2 nodes,
// the first with the original prefix up to atIdx, and the second with the
// value of the original node, and the original prefix after atIdx
// this function does not check index-in-range, this responsibility is
// left to the caller
func splitNode(atIdx int, n *pnode) (*pnode, *pnode) {
	newParent := newPNode(n.prefix[:atIdx], nil)
	newParent.addEdge(n)

	n.prefix = n.prefix[atIdx:]

	return newParent, n
}

// a node represents a location in the radix tree.
// it contains the additional characters (on top of that from the last node)
// which led to it, or its "prefix"
type pnode struct {
	prefix string
	val    interface{}
	edges  []*pnode
}

// newNode returns a new leaf node with the given value
func newPNode(k string, v interface{}) *pnode {
	return &pnode{
		prefix: k,
		val:    v,
		edges:  make([]*pnode, 0, defaultEdgeCount),
	}
}

// addEdge adds an edge to the given node
// this does not check for duplication,
// that responsibility lies with the caller
func (n *pnode) addEdge(other *pnode) {
	n.edges = append(n.edges, other)
}

func (n *pnode) put(k string, v interface{}) {
	// if there are edges, we search through them
	// linearly to find an edge we can continue down,
	// if one exists
	if len(n.edges) > 0 {
		// linearly search edges
		for i, enode := range n.edges {
			// give prefix a temp var name
			ep := enode.prefix

			// lengths of key & edge prefix, respectively
			lk, le := len(k), len(ep)

			// length of common prefix
			lp := prefixLen(k, ep)

			if lp == lk {
				// if the prefix length (lp) is the same as the length of the key,
				// we've found where we add the value
				if lp == le {
					// if lk == le && le == lp, i.e. if the prefix of the node
					// matches the key, we set the value and are done
					enode.val = v
				} else {
					// otherwise, we know lp < le, and so we split the node
					// the parent, i.e. the node with a prefix matching the key,
					// takes on the value we've been passed
					n.edges[i], enode = splitNode(lp, enode)
					n.edges[i].val = v
				}

				// we're done, return
				return
			} else if lp < lk {
				if lp == 0 {
					// no prefix match, move on
					continue
				}

				if lp == le {
					// we haven't used up our key yet, but there's a path to follow
					// so we consume the portion of the key we've matched and
					// continue down that path
					enode.put(k[lp:], v)
				} else {
					n.edges[i], enode = splitNode(lp, enode)
					n.edges[i].addEdge(newPNode(k[lp:], v))
				}

				// we're done, return
				return
			}
		}
	}

	// if we've not found an edge, or there are no edges,
	// we add a new edge for this key
	n.addEdge(newPNode(k, v))
}

func (n *pnode) get(k string) interface{} {
	if len(n.edges) > 0 {

		for _, enode := range n.edges {
			// give prefix a temp var name
			ep := enode.prefix

			// length of key
			lk := len(k)

			// length of common prefix
			lp := prefixLen(k, ep)

			if lp == lk {
				return enode.val
			} else if lp < lk {
				if lp == 0 {
					// no prefix match, move on
					continue
				}

				return enode.get(k[lp:])
			}
		}
	}

	return nil
}

// Patricia is a wrapper for a Patricia Trie wrapped with PNodes
type Patricia struct {
	root *pnode
}

func NewPatricia() *Patricia {
	return &Patricia{newPNode("", nil)}
}

func (p Patricia) Put(k string, v interface{}) {
	if k == "" {
		p.root.val = v
	} else {
		p.root.put(k, v)
	}
}

func (p Patricia) Get(k string) interface{} {
	if k == "" {
		return p.root.val
	} else {
		return p.root.get(k)
	}
}

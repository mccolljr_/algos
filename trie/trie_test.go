package trie

import "testing"

func TestTriePut(t *testing.T) {
	e := New()

	cases := []string{"a", "abc", "aba", "abb", "abba", "abra"}

	for i, c := range cases {
		e.Put(c, i)
	}

	for i, c := range cases {
		v := e.Get(c)

		if iv, ok := v.(int); !ok || iv != i {
			t.Errorf("Expected to get value  %d for %q, but got %d", i, c, iv)
		}
	}
}

// TODO structural tests
// TODO benchmarks

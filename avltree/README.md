[![goreportcard](https://goreportcard.com/badge/gitlab.com/mccolljr_/avltree "Go Report Card")](https://goreportcard.com/report/gitlab.com/mccolljr_/avltree)

# AVLTree
An implementation of an AVLTree in Go.
# Usage
Install with go get:
```
$ go get gitlab.com/mccolljr_/avltree
```
Then, include in your project:
```go
import "gitlab.com/mccolljr_/avltree"
```
Values for the AVLTree are stored in a `Comparer` interface:
```go
// Comparer is the interface used by the AVLTree implementation
// if A < B, A.Less(B) -> true
// if B <= A, A.Less(B) -> false
// equality is assumed when B.Less(A) and A.Less(B) are both false
type Comparer interface {
	Less(Comparer) bool
}
```
The package provides a Comparer wrapper for `int`, called `ComparableInt`:
```go
a := avltree.ComparableInt(10) // a.Less(avltree.ComparableInt(11)) -> true
```
`InsertOrUpdate` and `Delete` require an assignment, as they return a new root for the balanced tree:
```go
a := avltree.New()
a = a.InsertOrUpdate(avltree.ComparableInt(1))
a = a.Delete(avltree.ComparableInt(1))
```
Because of this, operations can be chained:
```go
a := avltree.New()
a = a.InsertOrUpdate(avltree.ComparableInt(10)).InsertOrUpdate(avltree.ComparableInt(15))
```
If you want to store complex data in the tree, you should use a struct, like so:
```go
type KVItem struct {
    Key int
    Value string
}

// implement the Comparer interface
func (kvi KVItem) Less(c avltree.Comparer) bool {
    return kvi.Key < c.(KVItem).Key
}

tree := avltree.New()
tree = tree.InsertOrUpdate(KVItem{1, "test"})
tree = tree.InsertOrUpdate(KVItem{1, "different"})
tree.Get(KVItem{Key: 1}) // -> KVItem{1, "different"}
```

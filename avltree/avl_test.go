package avltree

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestSingleLeftRotation(t *testing.T) {
	a := New()

	/*
		8					  9
		 \					 / \
		  9  should become  8   10
		   \
		    10

	*/

	a = a.InsertOrUpdate(ComparableInt(8)).
		InsertOrUpdate(ComparableInt(9)).
		InsertOrUpdate(ComparableInt(10))

	if a.value.(ComparableInt) != 9 ||
		a.left.value.(ComparableInt) != 8 ||
		a.right.value.(ComparableInt) != 10 {
		t.Logf("Expected [9 [l:8, r:10]] but got [%v [l:%v, r:%v]]", a.value, a.left.value, a.right.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestSingleRightRotation(t *testing.T) {
	a := New()

	/*
			10				   9
		   /				  / \
		  9	  should become  8   10
		 /
		8

	*/

	a = a.InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(9)).
		InsertOrUpdate(ComparableInt(8))

	if a.value.(ComparableInt) != 9 ||
		a.left.value.(ComparableInt) != 8 ||
		a.right.value.(ComparableInt) != 10 {
		t.Logf("Expected [9 [l:8, r:10]] but got [%v [l:%v, r:%v]]", a.value, a.left.value, a.right.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestDoubleLeftRotation(t *testing.T) {
	a := New()

	/*
		8					  9
		 \					 / \
		  10  should become 8   10
		 /
		9
	*/

	a = a.InsertOrUpdate(ComparableInt(8)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(9))

	if a.value.(ComparableInt) != 9 ||
		a.left.value.(ComparableInt) != 8 ||
		a.right.value.(ComparableInt) != 10 {
		t.Logf("Expected [9 [l:8, r:10]] but got [%v [l:%v, r:%v]]", a.value, a.left.value, a.right.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestDoubleRightRotation(t *testing.T) {
	a := New()

	/*
		  10				  9
		 /					 / \
		8	 should become  8   10
		 \
		  9

	*/

	a = a.InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(8)).
		InsertOrUpdate(ComparableInt(9))

	if a.value.(ComparableInt) != 9 ||
		a.left.value.(ComparableInt) != 8 ||
		a.right.value.(ComparableInt) != 10 {
		t.Logf("Expected [9 [l:8, r:10]] but got [%v [l:%v, r:%v]]", a.value, a.left.value, a.right.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestDeleteCaseLeaf(t *testing.T) {
	a := New()

	/*
				20
			  /    \
			10      30
		   /  \    /  \
		  5   12  25  40

		  becomes...

				20
			  /    \
			10      30
		      \    /  \
		      12  25  40
	*/

	a = a.InsertOrUpdate(ComparableInt(20)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(30)).
		InsertOrUpdate(ComparableInt(5)).
		InsertOrUpdate(ComparableInt(25)).
		InsertOrUpdate(ComparableInt(12)).
		InsertOrUpdate(ComparableInt(40)).
		// then, the deletion
		Delete(ComparableInt(5))

	if a.left.left != nil {
		t.Logf("expected node to be deleted, but got %v", a.left.left)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestDeleteCaseBypassLeft(t *testing.T) {
	a := New()

	/*
				20
			  /    \
			10      30
		   /       /  \
		  5       25  40

		  becomes...

				20
			  /    \
			 5      30
		           /  \
		          25  40
	*/

	a = a.InsertOrUpdate(ComparableInt(20)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(30)).
		InsertOrUpdate(ComparableInt(5)).
		InsertOrUpdate(ComparableInt(25)).
		InsertOrUpdate(ComparableInt(40)).
		// then, the deletion
		Delete(ComparableInt(10))

	if a.left.left != nil {
		t.Logf("expected node to be deleted, but got %v", a.left.left)
		t.Fail()
	}

	if a.left.value.(ComparableInt) != ComparableInt(5) {
		t.Logf("expected the new left child of root to be 5, but got %v", a.left.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestDeleteCaseBypassRight(t *testing.T) {
	a := New()
	/*
				20
			  /    \
			10      30
		   /  \       \
		  5   12      40

		  becomes...

				20
			  /    \
			10      40
		   /  \
		  5   12
	*/

	// one less insert than the other 2 deletion tests
	// skipping 25, to create a bypass delete condition
	a = a.InsertOrUpdate(ComparableInt(20)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(30)).
		InsertOrUpdate(ComparableInt(5)).
		InsertOrUpdate(ComparableInt(12)).
		InsertOrUpdate(ComparableInt(40)).
		// then, the deletion
		Delete(ComparableInt(30))

	if a.right.value.(ComparableInt) != 40 {
		t.Logf("expected value 40 where deleted value originally existed, but got %v", a.right.value)
		t.Fail()
	}

	if a.right.right != nil {
		t.Logf("expected the right subtree of root to only have height 1 due to bypass deletion, but got %v",
			a.right.right)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}

}

func TestDeleteCaseInternal(t *testing.T) {
	a := New()
	/*
				20
			  /    \
			10      30
		   /  \    /  \
		  5   12  25  40

		  becomes...

				12
			  /    \
			10      30
		   /       /  \
		  5       25  40
	*/

	a = a.InsertOrUpdate(ComparableInt(20)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(30)).
		InsertOrUpdate(ComparableInt(5)).
		InsertOrUpdate(ComparableInt(25)).
		InsertOrUpdate(ComparableInt(12)).
		InsertOrUpdate(ComparableInt(40)).
		// then, the deletion
		Delete(ComparableInt(20))

	if a.value.(ComparableInt) != 12 {
		t.Logf("expected new root to be 12, got %v", a.value)
		t.Fail()
	}

	if a.left.right != nil {
		t.Logf("expected 12's original placement to be deleted, but got %v in its place", a.left.right.value)
		t.Fail()
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestInOrder(t *testing.T) {
	a := New()

	a = a.InsertOrUpdate(ComparableInt(20)).
		InsertOrUpdate(ComparableInt(10)).
		InsertOrUpdate(ComparableInt(30)).
		InsertOrUpdate(ComparableInt(5)).
		InsertOrUpdate(ComparableInt(25)).
		InsertOrUpdate(ComparableInt(12)).
		InsertOrUpdate(ComparableInt(40))

	list := a.InOrder()

	switch false {
	case list[0].(ComparableInt) == ComparableInt(5):
	case list[1].(ComparableInt) == ComparableInt(10):
	case list[2].(ComparableInt) == ComparableInt(12):
	case list[3].(ComparableInt) == ComparableInt(20):
	case list[4].(ComparableInt) == ComparableInt(25):
	case list[5].(ComparableInt) == ComparableInt(30):
	case list[6].(ComparableInt) == ComparableInt(40):
	default:
		return
	}

	t.Logf("expected an in-order list, but got %s", list)
	t.Fail()
}

func TestLargeTreeIntegrity(t *testing.T) {
	a := New()

	for i := 0; i < 1000000; i++ {
		a = a.InsertOrUpdate(ComparableInt(rand.Int()))
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}

	for i := 0; i < 1000000; i++ {
		a = a.Delete(ComparableInt(rand.Int()))
	}

	if err := validate_tree(a); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func BenchmarkInsertOrUpdate(b *testing.B) {
	a := New()
	for i := 0; i < b.N; i++ {
		a = a.InsertOrUpdate(ComparableInt(i))
	}
}

func BenchmarkDelete(b *testing.B) {
	b.StopTimer()
	a := New()
	for i := 0; i < 1000000; i++ {
		a = a.InsertOrUpdate(ComparableInt(i))
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		a = a.Delete(ComparableInt(rand.Int()))
	}
}

func BenchmarkTreeValidation(b *testing.B) {
	b.StopTimer()
	a := New()
	for i := 0; i < 1000000; i++ {
		a = a.InsertOrUpdate(ComparableInt(i))
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		validate_tree(a)
	}
}

func BenchmarkInOrderTraversal(b *testing.B) {
	b.StopTimer()
	a := New()
	for i := 0; i < 1000000; i++ {
		a = a.InsertOrUpdate(ComparableInt(i))
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		a.InOrder()
	}
}

func BenchmarkComparableIntLessMethod(b *testing.B) {
	b.StopTimer()
	x, y := ComparableInt(rand.Int()), ComparableInt(rand.Int())
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		x.Less(y)
	}
}

func BenchmarkRegularInt(b *testing.B) {
	b.StopTimer()
	x, y := rand.Int(), rand.Int()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_ = x < y
	}
}

func validate_tree(a *AVLTree) error {
	return validate_tree_recursive(a, 0, nil)
}

func validate_tree_recursive(a *AVLTree, direction int, parent_value Comparer) error {
	if a == nil {
		return nil
	}

	lerr := validate_tree_recursive(a.left, -1, a.value)
	if lerr != nil {
		return lerr
	}

	rerr := validate_tree_recursive(a.right, 1, a.value)
	if rerr != nil {
		return rerr
	}

	if direction < 0 {
		if parent_value.Less(a.value) {
			return fmt.Errorf("Parent value should not be less than that of it's left child: %v < %v",
				parent_value, a.value)
		}
	} else if direction > 0 {
		if a.value.Less(parent_value) {
			return fmt.Errorf("Parent value should not be greater than that of it's right child: %v > %v",
				parent_value, a.value)
		}
	}

	oldl, oldr := a.lheight, a.rheight

	a.update_height()

	if oldl != a.lheight || oldr != a.rheight {
		return fmt.Errorf("Incorrect height found: got l: %v, r: %v, should have been l: %v, r: %v",
			a.lheight, a.rheight, oldl, oldr)
	}

	bal := a.balance_factor()
	if bal < -1 || bal > 1 {
		return fmt.Errorf("expected a balance factor between -1 and 1, got %d (lh: %d, rh: %d)\n",
			bal, a.lheight, a.rheight)
	}

	return nil
}

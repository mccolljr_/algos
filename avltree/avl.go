package avltree

// Comparer is the interface used by the AVLTree implementation
// if A < B, A.Less(B) -> true
// if B <= A, A.Less(B) -> false
// equality is assumed when B.Less(A) and A.Less(B) are both false
type Comparer interface {
	Less(Comparer) bool
}

// ComparableInt implements the Comparer interface for int
type ComparableInt int

// Less implements the Comparer interface's Less method
func (c ComparableInt) Less(other Comparer) bool {
	return (other.(ComparableInt) - c) > 0
}

// AVLTree represents an AVLTree Node (which is itself an AVLTree)
type AVLTree struct {
	right   *AVLTree
	left    *AVLTree
	lheight int
	rheight int
	value   Comparer
}

// New creates a new AVLTree and returns a pointer to it
func New() *AVLTree {
	return nil
}

// Get will return the value (as a Comparer) of the node that matches the given input
func (a *AVLTree) Get(v Comparer) Comparer {
	if a == nil {
		return nil
	}

	if v.Less(a.value) {
		return a.left.Get(v)
	}

	if a.value.Less(v) {
		return a.right.Get(v)
	}

	return a.value
}

// InsertOrUpdate will insert the value into the tree if it's not found,
// or update the value if it is found,
// and return the root of the balanced tree with the value added.
// It is necessary to assign the result of this function to the
// variable which stores the root of your tree
func (a *AVLTree) InsertOrUpdate(v Comparer) *AVLTree {
	return a.insert_assist(v)
}

// Delete will remove the given value from the tree, if found,
// and return the root of the balanced tree with the value deleted.
// It is necessary to assign the result of this function to the
// variable which stores the root of your tree
func (a *AVLTree) Delete(v Comparer) *AVLTree {
	return a.delete_assist(v)
}

// InOrder returns the elements of the tree as a sorted slice of Comparers
func (a *AVLTree) InOrder() []Comparer {
	list := make([]Comparer, 0, 1024*128)
	a.in_order_assist(&list)
	return list
}

func (a *AVLTree) in_order_assist(list *[]Comparer) {
	if a == nil {
		return
	}

	if a.left != nil {
		a.left.in_order_assist(list)
	}

	*list = append(*list, a.value)

	if a.right != nil {
		a.right.in_order_assist(list)
	}
}

func (a *AVLTree) insert_assist(v Comparer) *AVLTree {
	if a == nil {
		return &AVLTree{nil, nil, 0, 0, v}
	}

	// if the value belongs in the left subtree of a...
	if v.Less(a.value) {
		if a.left == nil {
			a.lheight++
			a.left = &AVLTree{nil, nil, 0, 0, v}
		} else {
			a.left = a.left.insert_assist(v)
			a.lheight = max(a.left.lheight, a.left.rheight) + 1
		}

		return a.balance()
	}

	// otherwise, the value belongs in the right subtree of a
	if a.value.Less(v) {
		if a.right == nil {
			a.rheight++
			a.right = &AVLTree{nil, nil, 0, 0, v}
		} else {
			a.right = a.right.insert_assist(v)
			a.rheight = max(a.right.lheight, a.right.rheight) + 1
		}

		return a.balance()
	}

	// We've found the value to exist,
	// so update the value and move on
	a.value = v
	return a
}

func (a *AVLTree) balance() *AVLTree {
	if a.left_heavy() {
		if a.left.balance_factor() > 0 {
			a.left = a.left.rotate_left()
		}

		return a.rotate_right()
	}

	if a.right_heavy() {
		if a.right.balance_factor() < 0 {
			a.right = a.right.rotate_right()
		}

		return a.rotate_left()
	}

	return a
}

func (a *AVLTree) delete_assist(v Comparer) *AVLTree {
	if a == nil {
		return nil
	}

	if v.Less(a.value) {
		a.left = a.left.delete_assist(v)
		a.update_height()
		return a.balance()
	}

	if a.value.Less(v) {
		a.right = a.right.delete_assist(v)
		a.update_height()
		return a.balance()
	}

	// now we've found the node to delete, do the deletion
	// CASE 1 -> Leaf Node
	if a.right == nil && a.left == nil {
		return nil
	} else if a.left == nil && a.right != nil {
		// CASE 2a -> Node w/ Single Child (Right)
		return a.right
	} else if a.left != nil && a.right == nil {
		// CASE 2b -> Node w/ Single Child (Left)
		return a.left
	}

	// CASE 3 -> Internal Node
	pred := a.get_predecessor()
	v2 := pred.value
	a.value = v2
	a.left = a.left.delete_assist(v2)
	a.update_height()
	return a.balance()
}

func (a *AVLTree) rotate_right() *AVLTree {
	root := a.left
	a.left = root.right
	root.right = a

	a.update_height()
	root.update_height()
	return root
}

func (a *AVLTree) rotate_left() *AVLTree {
	root := a.right
	a.right = root.left
	root.left = a

	a.update_height()
	root.update_height()
	return root
}

func (a *AVLTree) update_height() {
	if a == nil {
		return
	}

	if a.left == nil {
		a.lheight = 0
	} else {
		a.lheight = max(a.left.lheight, a.left.rheight) + 1
	}

	if a.right == nil {
		a.rheight = 0
	} else {
		a.rheight = max(a.right.lheight, a.right.rheight) + 1
	}
}

func (a *AVLTree) get_predecessor() *AVLTree {
	pred := a.left

	for pred.right != nil {
		pred = pred.right
	}

	return pred
}

func (a *AVLTree) balance_factor() int {
	if a == nil {
		return 0
	}

	return a.rheight - a.lheight
}

func (a *AVLTree) left_heavy() bool {
	return a.balance_factor() < -1
}

func (a *AVLTree) right_heavy() bool {
	return a.balance_factor() > 1
}

func equal(a, b Comparer) bool {
	return !a.Less(b) && !b.Less(a)
}

func max(vals ...int) int {
	max_val := vals[0]

	for _, v := range vals {
		if v > max_val {
			max_val = v
		}
	}

	return max_val
}

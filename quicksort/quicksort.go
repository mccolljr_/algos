package main

import (
	"fmt"
	"math/rand"
)

// IntSlice implements IFace for []int
type IntSlice []int

// Cmp is IFace's Cmp
func (s IntSlice) Cmp(i, j int) int {
	return s[i] - s[j]
}

// Len is IFace's Len
func (s IntSlice) Len() int {
	return len(s)
}

// Swap is IFace's Swap
func (s IntSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func main() {
	a := []int{}

	for i := 0; i < 10; i++ {
		d := rand.Int() % 1000
		a = append(a, d)
	}

	Quicksort3(IntSlice(a))

	fmt.Println(a)
}

// Quicksort3 performs a 3-parition quicksort
func Quicksort3(a IFace) {
	quicksort3(a, 0, a.Len()-1)
}

func quicksort3(a IFace, lo, hi int) {
	// 3-way QuickSort
	// ===============
	//
	// lower              upper
	// bound              bound
	//  |                   |
	// [x] [ ] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// Initially, the lower bound is at the first position in the partition,
	// and the upper bound is at the last position in the partition. X marks the
	// current position in our iteration
	//
	//
	// While x is at a position less than the upper bound...
	//
	// lower              upper
	// bound              bound
	//  |                   |
	// [x] [ ] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// NOTE: the first time the loop runs, x is at the same location as the pivot element,
	//          and so nothing happens but an increment in the position of x
	//
	// 1. is the value at position x less than the value of the pivot element?
	//
	//    if YES, swap the values at the position of x and the location of the pivot element,
	//    move the position of x forward by 1, move the position of the pivot element forward
	//    by 1, and restart the loop
	//
	//   ____ swap
	//  |   |
	//  V   V
	// [ ] [x] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// lower              upper
	// bound              bound
	//  |                   |
	// [ ] [ ] [x] [ ] [ ] [ ]
	//      |
	//     pivot
	//     element
	//
	// 2. is the value at position x greater than the value of the pivot element?
	//
	//    if YES, swap the values at the position of x and the position of the upper bound,
	//    decrease the position of the upper bound by 1, and restart the loop
	//
	//
	//   ____________________ swap
	//  |                   |
	//  V                   V
	// [x] [ ] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// lower           upper
	// bound           bound
	//  |               |
	// [x] [ ] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// 4. otherwise, move the position of x ahead by 1 and restart the loop
	//
	// lower              upper
	// bound              bound
	//  |                   |
	// [ ] [x] [ ] [ ] [ ] [ ]
	//  |
	// pivot
	// element
	//
	// END LOOP
	//
	// After the loop is completed, we'll have our parition organized like this:
	//
	//     1.       2.      3.
	//   <pivot  =pivot  >pivot
	//  ________________________
	// |       |       |       |
	//  [ ] [ ] [ ] [ ] [ ] [ ]
	//           |
	//         pivot
	//         element
	//
	// Anything in the 2nd partition, (=pivot), is considered sorted.
	// Now, we recursively call quicksory on paritions 1 and 3, like so:
	//
	// quicksort(<pivot)
	// quicksort(>pivot)
	//
	// The recursive call terminates when the partition is of length 1
	// (after all, how can you break a single element into paritions?)

	// base case of the recursion:
	// if the high/low pointers have crossed, we're done
	if hi <= lo {
		return
	}

	// less represents the location of the pivot
	// it is called less because any value
	// at an index below less is less than the
	// pivot value
	less := lo

	// greater is the index after which all values are
	// greater than the pivot value
	greater := hi

	// i moves between less and greater
	i := lo

	// while i <= the upper limit index of the partition...
	for i <= greater {
		// if a[i] < pivot...
		dir := a.Cmp(i, less)
		if dir < 0 {
			// swap the
			a.Swap(i, less)
			less++
			i++
		} else if dir > 0 {
			a.Swap(i, greater)
			greater--
		} else {
			i++
		}
	}

	quicksort3(a, lo, less-1)
	quicksort3(a, greater+1, hi)
}

package main

type IFace interface {
	Cmp(i, j int) int // <0: Item < other, 0: Item == other, >0: Item > other
	Swap(i, j int)
	Len() int
}
